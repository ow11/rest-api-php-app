CREATE DATABASE app_db;

USE app_db;

CREATE TABLE item(
    item_id BIGINT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
    title VARCHAR(255) NOT NULL,
    body TEXT NULL,
    state VARCHAR(100) NOT NULL
);

CREATE TABLE event(
    event_id BIGINT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
    datetime_s DATETIME NOT NULL,
    datetime_e DATETIME NOT NULL,
    item BIGINT UNSIGNED NOT NULL,

    FOREIGN KEY (item) REFERENCES item(item_id) ON DELETE CASCADE
);

INSERT INTO item (title, body, state) VALUES ('ToDo #1', 'Here is some description stored. Do not forget to come on time!', 'PLAN');
INSERT INTO item (title, body, state) VALUES ('ToDo #2', 'Here is another description stored. I should finish it soon, so move into DOING', 'DOING');
INSERT INTO event (datetime_s, datetime_e, item) VALUES ('2023-12-31 00:00', '2023-12-31 23:59', 1);