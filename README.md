# TODO/calendar organizer

### Monorepository for a full-stack web app written in php (+MySQL) and js

Contains of two parts: BE represented by API on php and FE on solidjs [not implemented yet].

Everything you need is packed into docker-compose - it contains all services needed.
You may want to run as following:
`docker-compose up -d --build`
to build and up the services,
but don't forget to create an .env file in the root directory of the project
(see .env-exmaple).
Also you will need to execute database.sql to initalize the database.

### Requirements

docker and docker-compose

To connect to the DB MySQL's >=8 client is needed (due to the new auth module)
To run the code locally PHP >=8 is vital
