import { createContext, createResource, createSignal, type ParentComponent } from 'solid-js';
import { createStore } from 'solid-js/store'
import { ItemsDto } from '../dto/ItemDto';
import { itemClient } from '../core/Clients';

export const GlobalContext = createContext<ItemsDto | undefined>()

export const GlobalContextProvider: ParentComponent = ( props ) => {

    const initialItems: ItemsDto | unknown = createResource(itemClient.getAllLoop)

    const [ items, setItems ] = createStore(initialItems || {
        data: [],
        totalCount: 0
    } )

    const value = [
        items,
        {
            update() {

            }
        }
    ]

    return (
        <GlobalContext.Provider value={[ items, setItems ]}>
            { props.children }
        </GlobalContext.Provider>
    )
}