import type { ParentComponent } from 'solid-js';

const Column: ParentComponent<{title: string}> = ( props ) => {
    return (
        <div>
            <h2>{ props.title }</h2>
            <div class="column">
                { props.children }
            </div>
        </div>
    )
}

export default Column