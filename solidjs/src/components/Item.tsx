import type { Component } from 'solid-js';

import { ItemDto } from '../dto/ItemDto';

const Item: Component<{ item: ItemDto }> = ( props ) => {
    return (
        <div id={ `item_${props.item.item_id}` }>
            <h3>{ props.item.title }</h3>
            <p>{ props.item.body || '' }</p>
            <button class="btn">Open</button>
        </div>
    )
}

export default Item