import { useContext, type Component } from 'solid-js';

import Item from '../components/Item'
import Column from '../components/Column'
import { GlobalContext } from '../context/GlobalContext';
import { ItemsDto } from '../dto/ItemDto';

const Kanban: Component = () => {
    const items: ItemsDto | undefined = useContext(GlobalContext)

    return (
        <div>
        <header>
            <h1>Kanban board</h1>
        </header>

        <Column title="TODO">
            <Item id={2} title="some text" body={null} />
            <Item id={2} title="some text" body={null} />
            <Item id={2} title="some text" body={null} />
            <Item id={2} title="some text" body={null} />
        </Column>

        </div>
    );
};

export default Kanban;
