export enum ItemState {
    TODO = 'TODO',
    DOING = 'DOING',
    DONE = 'DONE'
}

export interface CreateItemDto {
    title: string
    body: string | null
}

export interface UpdateItemDto {
    title: string
    body: string | null
    state: ItemState
}

export interface ItemDto {
    item_id: number
    title: string
    body: string | null
    state: ItemState
}

export interface ItemsDto {
    data: ItemDto[]
    totalCount: number
}
