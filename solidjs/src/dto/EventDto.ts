export type CreateEventDto = {
    datatime_s: string
    datatime_e: string
    item: number
}

export type UpdateEventDto = {
    datatime_s: string
    datatime_e: string
    item: number
}

export type EventDto = {
    event_id: number
    datatime_s: string
    datatime_e: string
    item: number
}

export interface EventsDto {
    data: EventDto[]
    totalCount: number
}

