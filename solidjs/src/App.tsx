import type { Component } from 'solid-js';
import { Routes, Route } from '@solidjs/router';

import Kanban from './pages/Kanban'
import Calendar from './pages/Calendar';
import ItemPage from './pages/ItemPage';

const App: Component = () => {
  return (
    <Routes>
      <Route path='/' component={Kanban} />
      <Route path='/calendar' component={Calendar} />
      <Route path='/item/:id' component={ItemPage} />
    </Routes>
  );
};

export default App;
