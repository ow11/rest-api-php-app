import { ConnectionSettings } from "../data_provider/ConnectionSettings";
import { IConnectionSettings } from "../data_provider/IConnectionSettings";

export const connectionSettings: IConnectionSettings = new ConnectionSettings('localhost', 8000, false)
