import { Client } from './Client'
import { IClient } from './IClient'

import { CreateItemDto, UpdateItemDto, ItemDto, ItemsDto } from '../dto/ItemDto'

export class ItemClient extends Client implements IClient {
    async getAll(limit: number=1000, offset: number=0): Promise<ItemsDto> {
        const relPath: string = `items?limit=${limit}&offset=${offset}`
        const response: JSON = await this.methodGet(relPath)
        return response as unknown as ItemsDto
    }

    async getAllLoop(): Promise<ItemsDto> {
        const items: ItemsDto = {
            data: [], totalCount: 0
        }
        const defaultLimit = 1000
        let lastCount = defaultLimit
        let offset = 0
        while( lastCount == defaultLimit ) {
            const result = await this.getAll(defaultLimit, offset)
            offset += defaultLimit
            lastCount = result.totalCount
            items.totalCount += lastCount
            items.data.push(...result.data)
        }
        return items
    }

    async get(id: number): Promise<ItemDto> {
        const relPath: string = `items/${id}`
        const response: JSON = await this.methodGet(relPath)
        return response as unknown as ItemDto
    }

    async create(data: CreateItemDto): Promise<Number> {
        const relPath: string = `items`
        const response: JSON = await this.methodPost(relPath, data)
        return response as unknown as { id: number }['id']
    }

    async update(id: number, data: UpdateItemDto): Promise<void> {
        const relPath: string = `items/${id}`
        await this.methodPut(relPath, data)
    }

    async delete(id: number): Promise<void> {
        const relPath: string = `items/${id}`
        await this.methodDelete(relPath)
    }
}