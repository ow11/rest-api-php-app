import { IConnectionSettings } from './IConnectionSettings'

export class ConnectionSettings implements IConnectionSettings{
    protected host: string
    protected port: number
    protected sslTrue: boolean

    constructor(host: string, port: number, sslTrue: boolean = false) {
        this.host = host
        this.port = port
        this.sslTrue = sslTrue
    }

    get uri() {
        const protocol: string = (this.sslTrue) ? 'https://' : 'http://'
        return protocol + this.host + ':' + this.port.toString() + '/'
    }
}
