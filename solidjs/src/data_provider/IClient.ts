export interface IClient {
    methodPost(relPath: string, data: any): Promise<JSON>
    methodGet(relPath: string): Promise<JSON>
    methodPut(relPath: string, data: any): Promise<JSON>
    methodDelete(relPath: string): Promise<JSON>
}