import { IConnectionSettings } from './IConnectionSettings'
import { IClient } from './IClient'

export class Client implements IClient {
    protected settings: IConnectionSettings

    constructor(settings: IConnectionSettings) {
        this.settings = settings
    }

    async methodPost(relPath: string, data: any): Promise<JSON> {
        const response = await fetch(this.settings.uri + relPath, {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify(data)
        })

        return response.json()
    }

    async methodGet(relPath: string): Promise<JSON> {
        const response = await fetch(this.settings.uri + relPath, {
            method: "GET",
            headers: {
                "Content-Type": "application/json"
            }
        })

        return response.json()
    }

    async methodPut(relPath: string, data: any): Promise<JSON> {
        const response = await fetch(this.settings.uri + relPath, {
            method: "PUT",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify(data)
        })

        return response.json()
    }

    async methodDelete(relPath: string): Promise<JSON> {
        const response = await fetch(this.settings.uri + relPath, {
            method: "DELETE",
            headers: {
                "Content-Type": "application/json"
            }
        })

        return response.json()
    }
}
