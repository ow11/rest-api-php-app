<?php

namespace App\Models;

use App\Core\IDbProvider;

class Model
{
    protected IDbProvider $db;

    public function __construct(IDbProvider $db_provider)
    {
        $this->db = $db_provider;
    }
}