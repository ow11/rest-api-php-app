<?php

namespace App\Models;

require __DIR__ . '/../../vendor/autoload.php';

use Faker\Factory as Faker;

class KanbanFakeModel implements IModelCRUD
{
    protected array $db;

    public function __construct()
    {
        $this->db = [];
        $faker = Faker::create();
        for ($i = 0; $i < 20; $i++)
            $this->db[] = ['id' => $i, 'title' => $faker->name()];
    } 

    public function readAll(int $offset, int $limit): ?array
    {
        return array_slice($this->db, $offset, $limit);
    }

    public function read(?array $params): mixed
    {
        $id = $params[0];
        return $this->db[$id] ?? NULL;
    }

    public function create(?array $data): ?int
    {
        $this->db[] = $data;
        return count($this->db)-1;
    }

    public function update(?array $params, ?array $data): void
    {
        $id = $params[0];
        $this->db[$id] = $data;
    }

    public function delete(?array $params): void
    {
        $id = $params[0];
        unset($this->db[$id]);
        $this->db = array_values($this->db);
    }
}