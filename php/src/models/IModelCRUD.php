<?php

namespace App\Models;

interface IModelCRUD
{
    public function readAll(int $offset, int $limit): ?array;
    public function read(?array $params): mixed;
    public function create(?array $data): ?int;
    public function update(?array $params, ?array $data): void;
    public function delete(?array $params): void;
}