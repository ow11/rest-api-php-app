<?php

namespace App\Models;

class KanbanModel extends Model implements IModelCRUD
{
    public function readAll(int $offset, int $limit): ?array
    {
        $con = $this->db->getConnection();
        $offset = $con->real_escape_string($offset);
        $limit = $con->real_escape_string($limit);
        $sql = "SELECT * FROM item LIMIT $limit OFFSET $offset";
        $con = $this->db->getConnection();
        $res = $con->query($sql);
        $con->close();
        $rows = [];
        while($row = $res->fetch_assoc())
            $rows[] = $row;
        return $rows;
    }

    public function read(?array $params): mixed
    {
        $con = $this->db->getConnection();
        $id = $con->real_escape_string($params[0]);
        $sql = "SELECT * FROM item WHERE item.item_id=$id";
        $res = $con->query($sql);
        $con->close();
        if ($res->num_rows === 0)
            return NULL;
        return $res->fetch_assoc();
    }

    public function create(?array $data): ?int
    {
        $con = $this->db->getConnection();
        $title = $con->real_escape_string($data['title']);
        $body = $con->real_escape_string($data['body'] ?? NULL);
        $state = "TODO";
        $sql = "INSERT INTO item (title, body, state) VALUES ('$title', '$body', '$state')";
        $res = $con->query($sql);
        if ($res !== true)
        {
            $con->close();
            return NULL;
        }
        $last_id = $con->insert_id;
        $con->close();
        return $last_id;
    }

    public function update(?array $params, ?array $data): void
    {
        $con = $this->db->getConnection();
        $id = $con->real_escape_string($params[0]);
        $title = $con->real_escape_string($data['title']);
        $body = $con->real_escape_string($data['body']);
        $state = $con->real_escape_string($data['state']);
        $sql = "UPDATE item SET title='$title', body='$body', state='$state' WHERE item.item_id=$id";
        $con->query($sql);
        $con->close();
    }

    public function delete(?array $params): void
    {
        $con = $this->db->getConnection();
        $id = $con->real_escape_string($params[0]);
        $sql = "DELETE FROM item WHERE item.item_id=$id";
        $con->query($sql);
        $con->close();
    }
}