<?php

namespace App\Models;

class EventModel extends Model implements IModelCRUD
{
    public function readAll(int $offset, int $limit): ?array
    {
        $con = $this->db->getConnection();
        $offset = $con->real_escape_string($offset);
        $limit = $con->real_escape_string($limit);
        $sql = "SELECT * FROM event LIMIT $limit OFFSET $offset";
        $res = $con->query($sql);
        $con->close();
        $rows = [];
        while($row = $res->fetch_assoc())
            $rows[] = $row;
        return $rows;
    }

    public function read(?array $params): mixed
    {
        $con = $this->db->getConnection();
        $id = $con->real_escape_string($params[0]);
        $sql = "SELECT * FROM event WHERE event.event_id=$id";
        $res = $con->query($sql);
        $con->close();
        if ($res->num_rows === 0)
            return NULL;
        return $res->fetch_assoc();
    }

    public function readByItemId(?array $params, int $offset, int $limit): ?array
    {
        $con = $this->db->getConnection();
        $offset = $con->real_escape_string($offset);
        $limit = $con->real_escape_string($limit);
        $id = $con->real_escape_string($params[0]);
        $sql = "SELECT event_id, datetime_s, datetime_e, event.item FROM item RIGHT JOIN event ON item.item_id=event.item WHERE event.item=$id LIMIT $limit OFFSET $offset";
        $res = $con->query($sql);
        $con->close();
        $rows = [];
        while($row = $res->fetch_assoc())
            $rows[] = $row;
        return $rows;
    }

    public function create(?array $data): ?int
    {
        $con = $this->db->getConnection();
        $datetime_s = $con->real_escape_string($data['datetime_s']);
        $datetime_e = $con->real_escape_string($data['datetime_e']);
        $item = $con->real_escape_string($data['item']);
        $sql = "INSERT INTO event (datetime_s, datetime_e, item) VALUES ('$datetime_s', '$datetime_e', $item)";
        $res = $con->query($sql);
        if ($res !== true)
        {
            $con->close();
            return NULL;
        }
        $last_id = $con->insert_id;
        $con->close();
        return $last_id;
    }

    public function update(?array $params, ?array $data): void
    {
        $con = $this->db->getConnection();
        $id = $con->real_escape_string($params[0]);
        $datetime_s = $con->real_escape_string($data['datetime_s']);
        $datetime_e = $con->real_escape_string($data['datetime_e']);
        $item = $con->real_escape_string($data['item']);
        $sql = "UPDATE item SET datetime_s='$datetime_s', datetime_e='$datetime_e', item='$item' WHERE event_id=$id";
        $con->query($sql);
        $con->close();
    }

    public function delete(?array $params): void
    {
        $con = $this->db->getConnection();
        $id = $con->real_escape_string($params[0]);
        $sql = "DELETE FROM event WHERE event_id=$id";
        $con->query($sql);
        $con->close();
    }
}