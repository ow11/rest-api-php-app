<?php

namespace App\View;

class View implements IView
{
    protected static function setHeaderJson(int $status_code): void
    {
        header("Content-type: application/json; charset=UTF-8", response_code: $status_code);
    }

    protected static function sendJson(int $statusCode, string|false $json): void
    {
        View::setHeaderJson($statusCode);

        if ($json == "null") {
            View::setHeaderJson(404);
            $json = NULL;
        }
        
        if ($json === false) {
            View::setHeaderJson(400);
            $json = NULL;
        }

        echo $json;
    }

    public static function create(string|false $json): void
    {
        View::sendJson(201, $json);
    }
    
    public static function read(string|false $json): void
    {
        View::sendJson(200, $json);
    }

    public static function update(string|false $json): void
    {
        View::sendJson(200, $json);
    }

    public static function delete(string|false $json): void
    {
        View::sendJson(204, $json);
    }

    public static function raise(): void
    {
        View::sendJson(400, false);
    }
}