<?php

namespace App\View;

interface IView
{
    public static function create(string|false $json): void;
    public static function read(string|false $json): void;
    public static function update(string|false $json): void;
    public static function delete(string|false $json): void;
    public static function raise(): void;
}