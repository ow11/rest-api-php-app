<?php

namespace App\Controllers;

use App\Models\IModelCRUD;
use App\Core\IRequest;
use App\View\View;

abstract class ControllerCRUD implements IControllerCRUD
{
    protected IModelCRUD $model;

    public function __construct(IModelCRUD $model)
    {
        $this->model = $model;
    }

    public function readAll(IRequest $request): void
    {
        $queries = $request->queries();
        $offset = $queries['offset'] ?? 0;
        $limit = $queries['limit'] ?? 1000;

        $db_result = $this->model->readAll($offset, $limit);

        $result = [
            'data' => $db_result,
            'totalCount' => count($db_result),
        ];

        $json = json_encode($result);

        View::read($json);
    }

    public function read(IRequest $request): void
    {
        $id = $request->params()[0];
        $db_result = $this->model->read([$id]);
        $json = json_encode($db_result);
        View::read($json);
    }
    
    public function create(IRequest $request): void
    {
        $json = $request->bodyJson();
        
        if (!$this->checkCreate($json))
        {
            View::raise();
            return;
        }

        $id = $this->model->create($json);
        View::create(json_encode(['id' => $id]));
    }

    public function update(IRequest $request): void
    {
        $id = $request->params()[0];
        $json = $request->bodyJson();
        
        if (!$this->checkUpdate($json))
        {
            View::raise();
            return;
        }

        $this->model->update([$id], $json);
        View::update(json_encode('status: OK'));
    }
    
    public function delete(IRequest $request): void
    {
        $id = $request->params()[0];
        $this->model->delete([$id]);
        View::delete(json_encode(['id' => $id]));
    }
    
    abstract protected function checkCreate(?array $json): bool;
    
    abstract protected function checkUpdate(?array $json): bool;
}