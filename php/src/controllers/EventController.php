<?php

namespace App\Controllers;

use App\Core\IRequest;
use App\View\View;

class EventController extends ControllerCRUD implements IControllerCRUD
{
    protected function checkCreate(?array $json): bool
    {
        return $this->check($json);
    }

    protected function checkUpdate(?array $json): bool
    {
        return $this->check($json);
    }

    protected function check(?array $json): bool
    {
        if (!$json) return false;
        if (!array_key_exists('datetime_s', $json) || !$json['datetime_s']) return false;
        if (!array_key_exists('datetime_e', $json) || !$json['datetime_e']) return false;
        if (!array_key_exists('item', $json) || !$json['item']) return false;
        return true;
    }

    public function readByItemId(IRequest $request): void
    {
        $id = $request->params()[0];
        $queries = $request->queries();
        $offset = $queries['offset'] ?? 0;
        $limit = $queries['limit'] ?? 1000;
        $db_result = call_user_func([$this->model, 'readByItemId'], [$id], $offset, $limit);
        $result = [
            'data' => $db_result,
            'totalCount' => count($db_result),
        ];
        $json = json_encode($result);
        View::read($json);
    }
}