<?php

namespace App\Controllers;

use App\Core\IRequest;

interface IControllerCRUD
{
    public function readAll(IRequest $request): void;
    public function read(IRequest $request): void;
    public function create(IRequest $request): void;
    public function update(IRequest $request): void;
    public function delete(IRequest $request): void;
}