<?php

namespace App\Controllers;

class KanbanController extends ControllerCRUD implements IControllerCRUD
{
    protected function checkCreate(?array $json): bool
    {
        if (!$json) return false;
        if (!array_key_exists('title', $json) || !$json['title']) return false;
        return true;
    }

    protected function checkUpdate(?array $json): bool
    {
        if (!$json) return false;
        if (!array_key_exists('title', $json) || !$json['title']) return false;
        if (!array_key_exists('state', $json) || !$json['state']) return false;
        return true;
    }
}