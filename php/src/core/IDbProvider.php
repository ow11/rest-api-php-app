<?php

namespace App\Core;

use mysqli;

interface IDbProvider
{
    public function __construct(
        string $host,
        string $database,
        string $username,
        string $password,
        int $port,
    );

    public function getConnection(): mysqli;
}