<?php

namespace App\Core;

use ErrorException;
use Throwable;

class DebugErrorHandler extends ErrorHandler implements IErrorHandler
{
    public static function handleException(Throwable $ex): void
    {
        header(500);
        echo json_encode([
            "code" => $ex->getCode(),
            "message" => $ex->getMessage(),
            "file" => $ex->getFile(),
            "line" => $ex->getLine(),
        ]);
    }
}