<?php

namespace App\Core;

use ErrorException;
use Throwable;
use App\View\View;

class ErrorHandler implements IErrorHandler
{
    public static function handleError(
        int $errno,
        string $errstr,
        string $errfile,
        int $errline
    ): void
    {
        throw new ErrorException($errstr, 0, $errno, $errfile, $errline);
    }
    public static function handleException(Throwable $ex): void
    {
        View::raise();
    }
}