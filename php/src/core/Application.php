<?php

namespace App\Core;

class Application implements IApplication
{
    protected IRouter $router;

    public function __construct(IRouter $router)
    {
        $this->router = $router;
    }

    public function run(): void
    {
        $this->router->resolve();
    }
}