<?php

namespace App\Core;

class Router implements IRouter
{
    protected IRequest $request;

    protected array $routes = [
        "GET" => [],
        "HEAD" => [],
        "POST" => [],
        "PUT" => [],
        "DELETE" => [],
        "CONNECT" => [],
        "OPTIONS" => [],
        "TRACE" => [],
        "PATCH" => [],
    ];

    public function set(string $path, string $method, callable|array $callback): void
    {
        $this->routes[$method][$path] = $callback;
    }

    protected function convertPath(): ?string
    {
        $path = $this->request->path();
        $method = $this->request->method();

        // Remove the closing slash if met
        if (str_ends_with($path, '/'))
            $path = rtrim($path, '/');

        // Compare if the provided path equals one of registered (has no parameters in URI)
        foreach ($this->routes[$method] as $key => $_)
        {
            if ($path === $key) return $path;
        }

        // Compare paths part by part
        foreach ($this->routes[$method] as $key => $_)
        {
            // Split paths into parts
            $parts = explode("/", $path);
            $registered_parts = explode("/", $key);

            // Check if both have the same length
            if (count($registered_parts) != count($parts)) continue;
            
            // Convert for further check
            foreach ($registered_parts as $i => $part)
            {
                if ($part == '[]') $parts[$i] = '[]';
            }

            if ($parts === $registered_parts)
            {
                $this->request->uri_pattern = $key;
                return $key;
            }
        }

        // NULL if not found
        return NULL;
    }

    public function resolve(): void
    {
        $this->request = new Request();

        $path = $this->convertPath();

        if (!$path) {
            http_response_code(404);
            return;
        }

        $method = $this->request->method();
        $callback = $this->routes[$method][$path];

        call_user_func($callback, $this->request);
    }
}