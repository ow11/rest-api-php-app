<?php

namespace App\Core;

interface IApplication
{
    public function __construct(IRouter $router);

    public function run(): void;
}