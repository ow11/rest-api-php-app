<?php

namespace App\Core;

Interface IRouter
{
    public function set(string $path, string $method, callable|array $callback): void;

    public function resolve(): void;
}