<?php

namespace App\Core;

interface IRequest
{
    public function __construct();

    public function path(): string;

    public function method(): string;

    public function queries(): ?array;

    public function params(): ?array;

    public function bodyJson(): ?array;
}