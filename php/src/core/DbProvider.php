<?php

namespace App\Core;

use mysqli;

class DbProvider implements IDbProvider
{
    public function __construct(
        private string $host,
        private string $database,
        private string $username,
        private string $password,
        private int $port,
    ) {}

    public function getConnection(): mysqli
    {
        $connection = new mysqli(
            $this->host,
            $this->username,
            $this->password,
            $this->database,
            $this->port,
        );

        if ($connection->connect_error) {
            die("Connection failed.");
        }

        return $connection;
    }
}