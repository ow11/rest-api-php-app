<?php

namespace App\Core;

use Throwable;

interface IErrorHandler
{
    public static function handleError(
        int $errno,
        string $errstr,
        string $errfile,
        int $errline
    ): void;
    public static function handleException(Throwable $ex): void;
}