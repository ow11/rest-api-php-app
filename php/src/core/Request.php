<?php

namespace App\Core;

class Request implements IRequest
{
    protected array $server;
    protected ?array $queries;
    protected ?string $body;
    public ?string $uri_pattern = NULL;

    public function __construct()
    {
        $this->server = $_SERVER;
        $this->queries = $_GET;
        $this->body = file_get_contents('php://input');
    }

    public function path(): string
    {
        $path = $this->server['REQUEST_URI'] ?? '/';
        $position = strpos($path, '?');

        if ($position === false) return $path;
        return substr($path, 0, $position);
    }

    public function method(): string
    {
        return strtolower($this->server['REQUEST_METHOD']);
    }

    public function queries(): ?array
    {
        return $this->queries;
    }

    public function params(): ?array
    {
        if (!$this->uri_pattern) return NULL;

        // Split paths into parts
        $parts = explode("/", $this->path());
        $registered_parts = explode("/", $this->uri_pattern);

        // Reulsting dict
        $params = [];

        // Find the parameters
        foreach ($registered_parts as $i => $part)
        {
            if ($part == '[]') $params[] = $parts[$i];
        }

        return $params;
    }

    public function bodyJson(): ?array
    {
        $json = json_decode($this->body, true);
        return $json;
    }
}