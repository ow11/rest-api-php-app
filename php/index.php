<?php

declare(strict_types=1);

require __DIR__ . '/vendor/autoload.php';

use App\Models;
use App\Controllers;
use App\Core\Router;
use App\Core\DbProvider;
use App\Core\Application;

// Use an appropriet error handler here (debug or not), which is set by env variable DEBUG
if ($_ENV["DEBUG"] == 1) {
    set_error_handler("App\Core\DebugErrorHandler::handleError");
    set_exception_handler("App\Core\DebugErrorHandler::handleException");
} else {
    set_error_handler("App\Core\ErrorHandler::handleError");
    set_exception_handler("App\Core\ErrorHandler::handleException");
}

// Init DbProvider
$db_provider = new DbProvider(
    "mysql",
    "app_db",
    "root",
    $_ENV["MYSQL_ROOT_PASSWORD"],
    3306,
);

// Init Models and controllers
$canban_model = new Models\KanbanModel($db_provider);
$event_model = new Models\EventModel($db_provider);

$canban_controller = new Controllers\KanbanController($canban_model);
$event_controller = new Controllers\EventController($event_model);

// Create router, register routes

$router = new Router();

$router->set("/", "GET", function() {
    var_dump($_SERVER);
});

$router->set("/items", "get", [$canban_controller, 'readAll']);

$router->set("/items/[]", "get", [$canban_controller, 'read']);

$router->set("/items/[]", "put", [$canban_controller, 'update']);

$router->set("/items", "post", [$canban_controller, 'create']);

$router->set("/items/[]", "delete", [$canban_controller, 'delete']);


$router->set("/items/[]/events", "get", [$event_controller, 'readByItemId']);

$router->set("/events", "get", [$event_controller, 'readAll']);

$router->set("/events/[]", "get", [$event_controller, 'read']);

$router->set("/events/[]", "put", [$event_controller, 'update']);

$router->set("/events", "post", [$event_controller, 'create']);

$router->set("/events/[]", "delete", [$event_controller, 'delete']);


// Create application, start listening

$app = new Application($router);
$app->run();